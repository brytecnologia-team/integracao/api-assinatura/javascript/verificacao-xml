module.exports = {

    // Request identifier
    NONCE: 1,

    // Identifier of the signature within a batch
    NONCE_OF_SIGNATURE: 1,

    // location where the signature is stored
    SIGNATURE_PATH: './assinatura-xml/AssinaturaXAdES-ADRC.xml',
}